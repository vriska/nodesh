const sh = require('.');

console.log(sh`echo ls \\| echo hi:`);
console.log(sh`ls` |> sh`echo hi`);
console.log(sh`echo ls \\| cat:`);
console.log(sh`ls` |> sh`cat`);
console.log(sh`echo ls \\| sed 's/\\./_dot_/g':`);
console.log(sh`ls` |> sh`sed 's/\\./_dot_/g'`);
