const shellEscape = require('shell-escape-tag');
const cp = require('child_process');
const util = require('util');

module.exports = function sh(...args) {
    const cmd = shellEscape(...args);

	const genObj = command => {
	    let toString = {toString: () => cp.execSync(command, {
    		encoding: 'utf8',
    	}), _cmd: command};
    	
    	toString.exec = toString.toString;
    	toString[util.inspect.custom] = toString.toString;
    	
    	let func = prevCmd => {
    	    let prevCmdStr = prevCmd._cmd;
    	    
    	    return genObj(`${prevCmdStr} | ${command}`);
    	};
    	
    	Object.assign(func, toString);
    	
    	return func;
	};
	
	return genObj(cmd);
}